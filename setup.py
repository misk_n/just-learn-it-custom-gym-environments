from setuptools import setup

setup(
    name='custom_jli_env',
    version='0.0.10',
    install_requires=[
        'gym',
        'numpy',
    ]
)
