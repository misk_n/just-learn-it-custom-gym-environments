from gym.envs.mujoco.ant import AntEnv
import numpy as np
import random


def get_rotation_matrix(angle):
    rotation = np.array(
        [
            [np.cos(angle), -np.sin(angle), 0],
            [np.sin(angle), np.cos(angle), 0],
            [0, 0, 1],
        ]
    )
    return rotation


class TargetAntEnv(AntEnv):
    def __init__(self):
        self.target = np.array([0, 0, 0])
        self.norm_vec = np.array([2, 0, 0])
        super().__init__()
        self.obs_dim += 3

        self.reset_target()


    def reset_target(self):
        angle = random.uniform(0.0, 2 * np.pi)
        rotation = get_rotation_matrix(angle)
        orig = self.get_body_com('torso')
        self.target = orig + rotation @ self.norm_vec

    def _get_obs(self):
        obs = super()._get_obs()
        print(self.__class__.__name__)
        return np.concatenate([
            obs,
            self.target,
        ])

    def reset_model(self):
        super().reset_model()
        self.reset_target()
