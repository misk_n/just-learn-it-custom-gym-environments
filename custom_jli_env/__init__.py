import logging
from gym.envs.registration import register

logger = logging.getLogger(__name__)

register(
    id='Target-Ant-v0',
    entry_point='custom_jli_env.mujoco:TargetAntEnv',
    timestep_limit=1000,
    reward_threshold=1.0,
    nondeterministic = True,
)
