# Custom JLI env

This repository contains a python plugin to be installed via pip command line tool.
It provides standard OpenAI gym environments for reinforcement learning.
For more information, see OpenAI Gym framework.

